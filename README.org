* acpi-brightness-scrips
  Collections of scripts to handle acpi video/brightness*
  events. These scrips will help you to use your brightness keys in
  minimal GNU/Linux distribution or in distribution that do not
  provide scripts to handle the brightness events.

  Currently, there are scrips only for Ati (not tested due lack of
  access to a laptop with Ati cards) and Intel integrated graphics.

* How to use
  Clone this repo and add the scrip to ~/etc/acpi/~, then create or
  modify (depends on the distribution) the brightnessdown and
  brightnessup event files as follows:

#+BEGIN_SRC conf
  # brightnessdown event file
  event=video/brightnessdown
  action=/etc/acpi/brightness.sh %e
#+END_SRC

#+BEGIN_SRC conf
  # brightnessup event file
  event=video/brightnessdown
  action=/etc/acpi/brightness.sh %e
#+END_SRC

* Script configuration
  You can change the following variables in the script to customize
  its behavior:

** ~minbrightness~
  Change it to any integer value. This variable is the threshold to
  stop reducing the brightness.

** ~step~
  Change it to any integer value. the constant rate to reduce or
  increase the brightness.

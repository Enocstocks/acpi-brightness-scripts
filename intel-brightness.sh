#!/bin/sh

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

brightness_path=/sys/class/backlight/intel_backlight
brightness_file=$brightness_path/brightness
maxbrightness=$( cat $brightness_path/max_brightness )
currentbrightness=$( cat $brightness_file )
minbrightness=10
step=10

case $1 in
    video/brightnessdown)
        currentbrightness=$(( $currentbrightness - $step ))

        if [ $currentbrightness -lt $minbrightness ] ; then
            echo $minbrightness > $brightness_file ;
        else
            echo $currentbrightness > $brightness_file ;
        fi
    ;;

    video/brightnessup)
        currentbrightness=$(( $currentbrightness + $step ))

        if [ $currentbrightness -gt $maxbrightness ] ; then
            echo $maxbrightness > $brightness_file ;
        else
            echo $currentbrightness > $brightness_file ;
        fi
    ;;
esac
